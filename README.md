# MYSQL/MariaDB SPLIT function #

### Setup
```
DROP function SPLIT;
CREATE FUNCTION `SPLIT`(
	str VARCHAR(255) ,
	delim VARCHAR(12) ,
	pos INT
) RETURNS VARCHAR(255) CHARSET utf8 

LANGUAGE SQL
  NOT DETERMINISTIC
  NO SQL
  
RETURN REPLACE(
	SUBSTRING(
		SUBSTRING_INDEX(str , delim , pos) ,
		CHAR_LENGTH(
			SUBSTRING_INDEX(str , delim , pos - 1)
		) + 1
	) ,
	delim ,
	''
);
```
### Test
```
SELECT SPLIT("hello crazy world", " ", 2) as test; -- > crazy
```
### Credits ###

* [Ramon Eijkemans](https://eikhart.com/blog/mysql-split-string)
* https://coderwall.com/p/zzgo-w/splitting-strings-with-mysql